Draft: Donation Recording and Financial Tracking

This is a Financial Tracking website built with react for the Community Church of Atascadero.\
As it stands, the production branch is using a depricated Google Firebase library, though if you use nodejs 8, it will work.\
The Master Branch is moving towards mongoDB.\
However, development has stalled as the need for the software has declined due to the Church not receiving cash donations due to the 2020 pandemic.