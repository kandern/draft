import React, { Component } from 'react';
import './Candy.css';
import {Donations, DonationsSidebar}from './Donations.js';
import {Patrons, PatronsSidebar}from './Patrons.js';
import firebase from "./database";
import {Redirect} from "react-router-dom";
class Home extends Component {


    constructor(props) {
        super(props);
        this.state={
            info:"home",
            currentView:"Donations"
        };
        this.callBack = this.callBack.bind(this);
        this.switchView = this.switchView.bind(this);
        this.reloadSidebar = this.reloadSidebar.bind(this);
    }

    switchView(event){
        this.setState({currentView:event.target.id});
    }

    //callback from the sidebar to change the content in the main section
    callBack(data){
        this.content.changeContent(data);
    }

    reloadSidebar(){
        this.sidebar.reload();
    }



    render(){
        /*let user = firebase.auth.currentUser;
        if (!user)
            return(<Redirect to={"/login"}/>);*/
        //determine what should go in the sidebar and content sections
        let Sidebar;
        let Content;
        switch (this.state.currentView) {
            case "Donations":
                Sidebar = <DonationsSidebar ref={donationsSidebar=>{this.sidebar = donationsSidebar}}
                                            callback={this.callBack}/>;
                Content = <Donations ref={donations=>{this.content = donations}} callback={this.reloadSidebar}/>;
                break;
            case "Patrons":
                Sidebar = <PatronsSidebar ref={patronsSidebar=>{this.sidebar = patronsSidebar}}
                                          callback={this.callBack}/>;
                Content = <Patrons ref={patrons=>{this.content = patrons}} callback={this.reloadSidebar}/>;
                break;
            case "Reports":
                Sidebar = <p>Reports</p>;
                Content = <p>Reports</p>;
                break;
            case "Organization":
                Sidebar = <p>Organization</p>;
                Content = <p>Organization</p>;
                break;
            default:
                Sidebar = <DonationsSidebar ref={donationsSidebar=>{this.sidebar = donationsSidebar}}
                                            callback={this.callBack}/>;
                Content = <Donations ref={donations=>{this.content = donations}} callback={this.reloadSidebar}/>;
        }

        return(
            <div className="full">
                <div id="header">
                    <div id="menu">
                        <button className="primary" id="Donations" onClick={this.switchView}>Donations/Payments</button>
                        <button className="primary" id="Patrons" onClick={this.switchView}>Patrons</button>
                        <button className="primary" id="Reports" onClick={this.switchView} disabled={true}>Reports</button>
                        <button className="primary" id="Organization" onClick={this.switchView} disabled={true}>Organization</button>
                    </div>
                </div>
                <span id="sidebar">
                    {Sidebar}
                </span>
                <span id="content">
                    {Content}
                </span>
                <div id="footer">
                    Draft Copyright 2019
                </div>
            </div>
        );
    }
}

export default Home;