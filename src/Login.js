import React, { Component } from 'react';
import './Login.css';
import firebase from "./firebase"
import {Redirect} from "react-router-dom";
class Login extends Component {

  constructor(props) {
    super(props);
    this.state={register:false,
                emailPlaceholder:"Email",
                passwordPlaceholder:"Password",
                codePlaceholder:"Access Code"};
    this.register = this.register.bind(this);
    this.cancel = this.cancel.bind(this);
    this.login = this.login.bind(this);
    this.updateLogin = this.updateLogin.bind(this);
    this.removePlaceholder = this.removePlaceholder.bind(this);
  }

  //When register button is hit attempt to register the account
  //If the code dialog is not available, show it so the next register click can register
  register(event){
    event.preventDefault();
    if(this.state.register===false){
      this.setState({register:true});
      return;
    }
    if(!this.state.email||!this.state.password||!this.state.code){
      this.setState({error:"Please fill out all three fields"});
      return;
    }

    //get document with key email. if it exists
    firebase.firestore.collection('users').doc(this.state.email).get().then(user=>{
      let code = user.data().code;
      //if the user entered the code firebase has register them and log them in
      console.log(code);
      if(this.state.code===code)
        firebase.auth.createUserWithEmailAndPassword(this.state.email,this.state.password).then(()=>{
          //we made a user, now login
          firebase.auth.signInWithEmailAndPassword(this.state.email,this.state.password).then(()=>{
            //force us to rerender i.e. redirect if all went well
            this.setState({login:true});
          }).catch(e=>{
            //failed to login after making user? probably can't connect to server
            this.setState({error:e.message});
          });
        }).catch(e=>{
          //we couldn't create user, but the user exists.
          this.setState({error:e.message});
        });
      else{
        this.setState({error:"The code entered is not correct"})
      }
    }).catch(
        e=>{
          //we couldn't create user, but the user exists.
          this.setState({error:e.message});
        }
    );
    //register account and remove code from firebase
  }

  //login If error print it
  login(event){
    console.log("logging in");
    event.preventDefault();
    firebase.auth.signInWithEmailAndPassword(this.state.email,this.state.password).then(()=>{
      //force us to rerender i.e. redirect if all went well
      this.setState({login:true});
    }).catch(e=>{
      //failed to login after making user? probably can't connect to server
      this.setState({error:e.message});
    });
  }

  //remove the code input box
  cancel(event){
    event.preventDefault();
    this.setState({
      register:false
    });
  }

  //anytime any text is entered update whichever value is being edited
  updateLogin(event){
    let id = event.target.id;
    let newState = {
      email:this.state.email,
      password:this.state.password,
      code:this.state.code,
    };
    if(id==="email")
      newState.email = event.target.value;
    else if (id==="password")
      newState.password = event.target.value;
    else if (id==="code")
      newState.code = event.target.value;

    this.setState(newState);
  }

  //when a user clicks in an input box, remove the placeholder
  removePlaceholder(event){
    let type = event.target.id;
    let newPlaceholders = {
      emailPlaceholder:"Email",
      passwordPlaceholder:"Password",
      codePlaceholder:"Access Code"
    };

    if(type==="email")
      newPlaceholders.emailPlaceholder = "";
    else if (type==="password")
      newPlaceholders.passwordPlaceholder = "";
    else if (type==="code")
      newPlaceholders.codePlaceholder = "";
    this.setState(newPlaceholders);
  }

  render() {
    //check if the user has been logged in If so, redirect
    let user = firebase.auth.currentUser;
    if (user)
      return(<Redirect to={"/home"}/>);


    if(this.state.register === true){
      return (
          <div className="login">
            <h1>Draft</h1>
            <h5>Donation Recording And Finance Tracking</h5>
            <form>
              <input id="email" className="top" type="text"  placeholder={this.state.emailPlaceholder}
                     value={this.state.email} onFocus={this.removePlaceholder} onChange={this.updateLogin}/>
              <input id="password" className="middle" type="password" placeholder={this.state.passwordPlaceholder}
                     value={this.state.password} onFocus={this.removePlaceholder} onChange={this.updateLogin}/>
              <input id="code" className="bottom" type="text" placeholder={this.state.codePlaceholder}
                     value={this.state.code} onFocus={this.removePlaceholder} onChange={this.updateLogin}/>
              <p id="error" hidden={this.state.error===undefined}>{this.state.error}</p>
              <br/>
              <input className="button" type="submit" value="Register" onClick={this.register}/>
              <input className="button" type="submit" value="Cancel" onClick={this.cancel}/>
            </form>

          </div>

      );
    }else
    return (
        <div className="login">
          <h1>Draft</h1>
          <h5>Donation Recording And Finance Tracking</h5>
          <form>
            <input id="email" className="top" type="text" placeholder={this.state.emailPlaceholder}
                   value={this.state.email} onFocus={this.removePlaceholder} onChange={this.updateLogin}/>
            <input id="password" className="bottom" type="password" placeholder={this.state.passwordPlaceholder}
                   value={this.state.password} onFocus={this.removePlaceholder} onChange={this.updateLogin}/>
            <p id="error" hidden={this.state.error===undefined}>{this.state.error}</p>
            <br/>
            <input className="button" type="submit" value="Login" onClick={this.login}/>
            <input className="button" type="submit" value="Register" onClick={this.register}/>
          </form>

        </div>

    );
  }
}

export default Login;
