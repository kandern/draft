import React, { Component } from 'react';
import firebase from "./database"
import './Candy.css';
import './Donations.css';

class Donations extends Component {

    constructor(props) {
        super(props);
        this.state={
            new:true,
            donation:{date:"",patron:"",fund:"",amount:""},
            donationID:"",
            currentView:"Donations",
            donations:[],
            funds:[],
            patrons:{},
            valid:false,
            callback:props.callback.bind(this)
        };

        this.submit = this.submit.bind(this);
        this.updateDonation = this.updateDonation.bind(this);
        this.updatePatron = this.updatePatron.bind(this);
    };

    componentWillMount(){
        if(this.state.new){
        //get patrons
            firebase.firestore.collection('patrons').get().then(docs=>{
                let patrons = {};
                docs.forEach(doc=>{
                    patrons[doc.id] = doc.data();
                });
                let donation = this.state.donation;
                donation.patron = Object.keys(patrons)[0];
                let patron = patrons[donation.patron].fName + ' ' + patrons[donation.patron].lName;
                this.setState({patrons:patrons,donation:donation,patron:patron});
            });

            //get list of funds
            firebase.firestore.collection('organization').doc("categories").get().then(doc=>{
                let categories = doc.data().funds;
                let donation = this.state.donation;
                donation.fund = categories[0];
                this.setState({funds:categories,donation:donation});
            });

            firebase.firestore.collection('donations').get().then(docs=>{
                //get list of patrons

                    //apply the names to the donations
                    let donations = {};
                    docs.forEach(doc=>{
                        let donation = doc.data();
                        //find the patron specified in the donation
                        donations[doc.id]=donation;
                    });
                    this.setState({donations:donations,currentList:donations});
            });

        }
    }

    updateDonation(event){
        let newState = this.state;
        if(newState.donation===undefined)
            newState.donation= {};
        newState.donation[event.target.id] = event.target.value;
        newState.valid = true;

        //if the firstName section is empty then make the operation invalid
        for(let key in newState.donation){
            if(newState.donation.hasOwnProperty(key))
                if(newState.donation[key]==="")
                    newState.valid = false;
        }
        this.setState(newState);
    }

    updatePatron(event){
        let newState = this.state;
        if(newState.donation===undefined)
            newState.donation= {};
        newState.donation.patron = event.target.id;

        //validate
        newState.valid = true;
        //if the firstName section is empty then make the operation invalid
        for(let key in newState.donation){
            if(newState.donation.hasOwnProperty(key))
                if(newState.donation[key]==="")
                    newState.valid = false;
        }

        this.setState(newState);
    }

    //given data from the sidebar update the donation
    changeContent(donation){
        let newState = this.state;
        //if we get undefined, the user clicked add new
        if(donation===undefined){
            newState.valid = false;
            newState.new = true;
            newState.donation = {date:"",patron:"",fund:"",amount:""};
            newState.donationID = "";
            newState.patron = "";
            this.setState(newState);
        }else{
            newState.valid = true;
            newState.new = false;
            newState.donation = this.state.donations[donation];
            newState.donationID = donation;
            newState.patron = this.state.patrons[newState.donation.patron].fName + ' ' +
                this.state.patrons[newState.donation.patron].lName;
            this.setState(newState);
        }
    }

    submit(){
        console.log(this.state.new);
        if(this.state.new)
            firebase.firestore.collection("donations").add(
                this.state.donation).then(doc =>{
                this.setState({donation:doc.id,new:false});
                this.state.callback();
            });
        else
            firebase.firestore.collection("donations").doc(this.state.donationID).set(
                this.state.donation).then(()=>{
                this.state.callback();
            });

    }

    render() {
        let count = 0;
        let patrons = [];
        for (let key in this.state.patrons) {
            if(this.state.patrons.hasOwnProperty(key)){
                let patron = this.state.patrons[key].fName + ' ' + this.state.patrons[key].lName;
                patrons.push(<option value={patron} id={key} key={count++} onClick={this.updatePatron}>{patron}</option>);
            }
        }

        let funds = [];
        if(this.state.funds!==undefined)
            this.state.funds.forEach( fund =>{
                funds.push(<option value={fund} key={count++}>{fund}</option>);
            });

        return(
        <div className="container">
            <div id="centered">
                <label>Date</label>
                <input id="date" type="date" value={this.state.donation.date} onChange={this.updateDonation}/>
                <br/>
                <label>Patron</label>
                <select id="patron" value={this.state.patron}>{patrons}</select>
                <br/>
                <label>Fund</label>
                <select id="fund" onChange={this.updateDonation} value={this.state.donation.fund}>{funds}</select>
                <br/>
                <label>Amount</label>
                <input id="amount" type="number" placeholder="25" value={this.state.donation.amount} onChange={this.updateDonation}/>
                <br/>
                {this.state.new?<button id="submit" className="primary" disabled={!this.state.valid} onClick={this.submit}>Create</button>:
                    <button id="submit" className="primary" disabled={!this.state.valid} onClick={this.submit}>Change</button>}
            </div>
        </div>
        );


    }
}

class DonationsSidebar extends Component {

    constructor(props) {
        super(props);
            this.state={
                info:"side",
                callback:props.callback.bind(this),
                search:""
            };
        this.getSublist = this.getSublist.bind(this);
        this.reload = this.reload.bind(this);
    }

    //used for when a new donation is made
    reload(){
        //get document with key email. if it exists
        firebase.firestore.collection('donations').get().then(docs=>{
            //get list of patrons
            let patrons = {};
            firebase.firestore.collection('patrons').get().then(patronDocs=>{
                patronDocs.forEach(patron=>{
                    patrons[patron.id] = patron.data().fName + " " + patron.data().lName;
                });

                //apply the names to the donations
                let donations = {};
                docs.forEach(doc=>{
                    let donation = doc.data();
                    //find the patron specified in the donation
                    if(patrons[donation.patron]!==undefined)
                        donation.name = patrons[donation.patron] + " - " + donation.date + " : $" +  donation.amount;
                    donations[doc.id]=donation;
                });
                this.setState({donations:donations,currentList:donations,search:this.state.search});
                this.getSublist(this.state.search);
            });

        }).catch(
            e=>{
                //we couldn't create user, but the user exists.
                this.setState({error:e.message});
            }
        );
    }

    //retrieve all the donations from the database
    componentWillMount() {
        //get document with key email. if it exists
        firebase.firestore.collection('donations').get().then(docs=>{
            //get list of patrons
            let patrons = {};
            firebase.firestore.collection('patrons').get().then(patronDocs=>{
                patronDocs.forEach(patron=>{
                    patrons[patron.id] = patron.data().fName + " " + patron.data().lName;
                });

                //apply the names to the donations
                let donations = {};
                docs.forEach(doc=>{
                    let donation = doc.data();
                    //find the patron specified in the donation
                    if(patrons[donation.patron]!==undefined)
                        donation.name = patrons[donation.patron] + " - " + donation.date + " : $" +  donation.amount;
                    donations[doc.id]=donation;
                });
                this.setState({donations:donations,currentList:donations});
            });

        }).catch(
            e=>{
                //we couldn't create user, but the user exists.
                this.setState({error:e.message});
            }
        );
    }

    getSublist(search){
        let currentList = {};
        if(search===""||search===undefined){
            this.setState({currentList:this.state.donations,search:search});
            return;
        }
        for(let key in this.state.currentList){
            let donation = this.state.currentList[key];
            for(let prop in donation){
               if(donation.hasOwnProperty(prop))
               if(prop!=="patron"&&donation[prop].toString().toUpperCase().includes(search.toUpperCase())){
                   currentList[key] = donation;
                   break;
               }
           }
        }
        this.setState({currentList:currentList,search:search})
    }

    render(){
        let count = 0;
        let donations = [];
        //make list of donations
        if(this.state.donations === undefined){
            donations.push(<p key="loading">Loading...</p>);
        }else
            for(let key in this.state.currentList){
                let donation = this.state.currentList[key];
                donations.push(<button className="item" id={key} key={key}
                               onClick={event=>{this.state.callback(event.target.id)}}>{donation.name}</button>);
                donations.push(<br key={donation.name + "br" + count++}/>);
            }

        return(
            <div>
                <div id="search">
                    <input type="text" placeholder="Search e.g. date, patron, amount"
                           onChange={event =>{this.getSublist(event.target.value)}} value={this.state.search}/>
                    <button id="add" onClick={()=>{this.state.callback()}}>Add New</button>
                </div>
                <div id="scrollable">
                    {donations}
                </div>
            </div>);
    }

}

export {Donations,DonationsSidebar};
