import React, { Component } from 'react';
import firebase from "./database"
import './Candy.css';
import './Patrons.css';

class Patrons extends Component {

    constructor(props) {
        super(props);
        this.state={
            new:true,
            currentView:"Donations",
            mode:props.mode,
            donation:props.donation,
            patrons:["bob","jim","jess","kate"],
            valid:false,
            patron:"",
            user:{fName:"",lName:"",address1:"",address2:"",city:"",state:"",zipCode:"",category:"Pledger",pledge:""},
            callback:this.props.callback.bind(this)
        };
        this.updateUser = this.updateUser.bind(this);
        this.changeContent = this.changeContent.bind(this);
        this.submit = this.submit.bind(this);
    };

    //get different types of patrons
    componentWillMount() {
        firebase.firestore.collection('organization').doc("categories").get().then(doc=>{
            let categories = doc.data().patronTypes;

            this.setState({categories:categories});
        });
        firebase.firestore.collection('patrons').get().then(docs=>{
            let patrons = {};
            docs.forEach(doc=>{
               patrons[doc.id] = doc.data();
            });
            this.setState({patrons:patrons});
        });
    }

    //given data from the sidebar update the donation
    changeContent(patron){
        console.log(patron);
        let newState = this.state;
        //if we get undefined, the user clicked add new
        if(patron===undefined){
            newState.valid = false;
            newState.new = true;
            newState.user = {fName:"",lName:"",address1:"",address2:"",city:"",
                state:"",zipCode:"",category:"Pledger",pledge:""};
            newState.patron = "";
            this.setState(newState);
        }else{
            newState.valid = true;
            newState.new = false;
            newState.patron = patron;
            newState.user = this.state.patrons[patron];
           // newState.patron = this.state.donations[donation];
          //  newState.patron = this.state.patrons[newState.donation.patron].fName + ' ' +
         //       this.state.patrons[newState.donation.patron].lName;
            this.setState(newState);
        }
    }

    updateUser(event){
        let newState = this.state;
        if(newState.user===undefined)
            newState.user = {};
        newState.user[event.target.id] = event.target.value;
        //if the firstName section is empty then make the operation invalid
        newState.valid = !(newState.user.fName === "" || newState.user.fName === undefined);

        this.setState(newState);
    }

    //update whichever the current user is in the database
    submit(){
        if(this.state.new)
            firebase.firestore.collection("patrons").add(
                this.state.user).then(doc =>{
                this.setState({patron:doc.id,new:false});
                this.state.callback();
            });
        else
            firebase.firestore.collection("patrons").doc(this.state.patron).set(
                this.state.user).then(()=>{
                    this.state.callback();
                });

    }

    render(){
        let user = this.state.user;
        //if no user, make empty user
        let categories = [];
        if(this.state.categories!==undefined)
        for(let category of this.state.categories)
            categories.push(<option value={category} key={category}>{category}</option>);
        return(
        <div className="container">
            <div id="centered">
                <label>First Name</label>
                <input id="fName" type="text" name="fName" value={user.fName} placeholder="John" onChange={this.updateUser}/>
                <label>Last Name</label>
                <input id="lName" type="text" name="lName" value={user.lName} placeholder="Doe" onChange={this.updateUser}/>
                <div className="inline">
                    <label >Address</label>
                    <br/>
                    <input id="address1" type="text" name="address" value={user.address1}
                           placeholder="5850 Rosario Avenue" onChange={this.updateUser}/>
                    <br/>
                    <input id="address2" type="text" name="address2" value={user.address2}
                           placeholder="Apt#/PO box" onChange={this.updateUser}/>
                </div>
                <div className="inline">
                    <label >City </label>
                    <br/>
                    <input type="text" id="city" placeholder="Atascadero" value={user.city} onChange={this.updateUser}/>
                    <br/>
                    <label>State </label>
                    <input type="text" id="state" placeholder="CA" value={user.state} onChange={this.updateUser}/>
                </div>
                <br/>
                <label>Zip code</label>
                <input type="text" id="zipCode" placeholder="93422" value={user.zipCode} onChange={this.updateUser}/>
                <br/>
                <label>Member Type</label>
                <select id="category" value={user.category} onChange={this.updateUser}>{categories}</select>
                <label id="pledgeLabel">Pledge</label>
                <input type="text" id="pledge" placeholder="1000" value={user.pledge} onChange={this.updateUser}/>
                <br/>
                {this.state.new?<button id="submit" className="primary" disabled={!this.state.valid} onClick={this.submit}>Create</button>:
                    <button id="submit" className="primary" disabled={!this.state.valid} onClick={this.submit}>Change</button>}

            </div>
        </div>
        );


    }
}

class PatronsSidebar extends Component {

    constructor(props) {
        super(props);
        this.state={
            callback:props.callback.bind(this),
            search:""
        };
        this.onClick = this.onClick.bind(this);
        this.getSublist = this.getSublist.bind(this);
        this.reload = this.reload.bind(this);
    }

    //used when a new patron is added
    reload(){
        let patrons = {};
        firebase.firestore.collection('patrons').get().then(patronDocs=>{
            patronDocs.forEach(doc=>{
                patrons[doc.id] = doc.data();
            });
            this.setState({patrons:patrons,currentList:patrons});
            this.getSublist(this.state.search);
        }).catch(error=>{
            console.log(error);
        });
    }

    //retrieve all the Patrons from the database
    componentWillMount() {
        let patrons = {};
        firebase.firestore.collection('patrons').get().then(patronDocs=>{
            patronDocs.forEach(doc=>{
                patrons[doc.id] = doc.data();
            });
            this.setState({patrons:patrons,currentList:patrons});
        }).catch(error=>{
            console.log(error);
        });
    }

    onClick(event){
        if(event.target.id === "add"){
            this.setState({
                date:"",
                patron:"",
                fund:"",
                value:""
            });
        }else{
            let id = event.target.id;
            this.setState({
                date:this.state[id].date,
                patron:this.state[id].patron,
                fund:this.state[id].fund,
                value:this.state[id].value
            });
        }
    }

    getSublist(search){
        let currentList = {};
        if(search===""||search===undefined){
            this.setState({currentList:this.state.patrons,search:search});
            return;
        }
        for(let key in this.state.patrons){
            let patron = this.state.patrons[key];
            for(let prop in patron)
                if(patron.hasOwnProperty(prop))
                    if(patron[prop].toString().toUpperCase().includes(search.toUpperCase())){
                        currentList[key] = patron;
                        break;
                    }
        }
        this.setState({currentList:currentList,search:search})
    }

    render(){
        let patrons = [];
        if(this.state.patrons === undefined){
            patrons.push(<p key={"Loading"}>Loading...</p>);
        }else
            for(let key in this.state.currentList){
                let patron = this.state.currentList[key];
                patrons.push(<button className="item" key={key} id={key}
                                     onClick={event=>{this.state.callback(event.target.id)}}>
                    {patron.fName + ' ' + patron.lName}</button>);
                patrons.push(<br key={key + "br"}/>);
            }

        return(
            <div>
                <div id="search">
                    <input type="text" placeholder="Search e.g. name, Type, address,"
                           onChange={event=>{this.getSublist(event.target.value)}} value={this.state.search}/>
                    <button id="add" onClick={()=>{this.state.callback()}}>Add New</button>
                </div>
                <div id="scrollable">
                    {patrons}
                </div>
            </div>);
    }

}

export {Patrons,PatronsSidebar};