import React, { Component } from 'react';
import {BrowserRouter,Route, Switch} from "react-router-dom";
import Login from "./Login";
import Home from "./Home";
import auth from "./firebase";

class Main extends Component {

    constructor(props) {
        super(props);
        this.state={user:{}};
    }

    render() {
        //if the user is logged in, then we should send them to the home page instead of the Login page
        let mainPage = auth.currentUser?Home:Login;
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={mainPage} exact/>
                    <Route path="/login" component={Login}/>
                    <Route path="/home" component={Home}/>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default Main;